summary: Reports back on the Kernel Installation that was done
description: |
    This task does following:
    1. installs new kernel
    1a. via yum if possible
    1b. directly from brew via http, this includes brewroot and kernelarchive

    If any of above fails, whole recipe gets aborted.
    2. sets new kernel as default
       either directly by editing boot config or on new distros by using grubby
    3. makes reboot
    4. submit logs, including output from lspci, lsmod, ip link show, ethtool, etc.

    You must pass three testarg environment variables:

    KERNELARGNAME
    KERNELARGVERSION
    KERNELARGVARIANT

    KERNELARGNAME is name of package, for example: 'kernel', 'kernel-aarch64', 'kernel-pegas'
    KERNELARGVERSION is the version-release
    KERNELARGVARIANT is variant: up, xen, debug
    "up" in newer distros will be equivalent to SMP.

    You CAN optionally pass KERNELARGTMPREPO and KERNELARGPERMREPO environment
    variables:

    KERNELARGTMPREPO="http://download-node-02.eng.bos.redhat.com/rel-eng/latest-RHEL-7/compose/Server/x86_64/os"
    KERNELARGTMPREPO="baseurl1 baseurl2"

    This variable contains a SPACE SEPARATED list of REPO URLS. This task will
    create a TEMPORARY repository just for the purpose of this task.  The
    repository will be disabled at the end of the task.

    KERNELARGPERMREPO="baseurl1 baseurl2"

    This variable contains a SPACE SEPARATED list of REPO URLS. In contrast with
    repositories defined with KERNELARGTMPREPO this will create a PERMANENT
    repository which will stay enabled even after kernel install task will finish.

    You can optionally pass KERNELARGEXTRAMODULES environment variable:
    KERNELARGEXTRAMODULES="1"

    If this variable is set to 1 then the kernel-modules-extra will be installed
    along with kernel-modules rpm. On RHEL8 and later distributions the extra
    package is not installed by default.
contact: Jeff Bastian <jbastian@redhat.com>
test: bash ./runtest.sh
framework: shell
require:
  - iproute
  - ethtool
  - kernel-headers
duration: 120m
extra-summary: /kernel/distribution/kernelinstall
extra-task: /kernel/distribution/kernelinstall
