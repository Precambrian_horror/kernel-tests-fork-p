# storage/nvdimm/fsdax_dm_linear

Storage: nvdimm fsdax dm_linear test

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
