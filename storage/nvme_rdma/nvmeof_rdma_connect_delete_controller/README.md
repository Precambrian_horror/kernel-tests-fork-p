# storage/nvme_rdma/nvmeof_rdma_connect_delete_controller

Storage: nvmeof rdma connect delete_controller stress test

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
